# Test Case Generation for Dialogflow Task-Based Chatbots
Chatbots are software, typically embedded in Web and Mobile applications, designed to assist the user in a plethora of activities, from chit-chatting to tasks achievement, enabling diverse forms of interactions, like text and voice commands. 
As any software, even chatbots are susceptible to bugs, and their pervasiveness in our lives, as well as the underlying technological advancements, have arisen the need for tailored quality assurance techniques. 
However, test case generation techniques for conversational chatbots are still limited.

We have conducted an experiment comparing our solution to task-based chatbot testing, named Chatbot Tests Generator (CTG), with the state-of-the-art Botium and Charm tools. Our findings show that the test cases generated by CTG outperformed the competitors, in terms of both robustness and effectiveness.


This repository contains the resources needed to replicate our experiment. It is structured as follows:
- **Chatbots:** This folder contains the 7 Dialogflow chatbots selected for the experiment;
- **Mutants:** This folder contains the mutants of each chatbot used in mutants detection analysis;
- **Tests:** This folder contains the tests produced by the three competing techniques;
- **Tools:** This folder contains CTG (Case Test Generation) tool, along with usage details and references to Botium and Charm competing techniques.